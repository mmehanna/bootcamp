const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
let appEnv = "./" + (process.env.NODE_ENV || "development") + ".json";
process.env.NODE_ENV = appEnv;
let config = require(appEnv);
let url = "mongodb://" + config.mongo.host + ":" + config.mongo.port + "/" + config.mongo.dbName;

let connectMongo = (next) => MongoClient.connect(url, (err, db) => next(err, db));

module.exports = {
    connectMongo: connectMongo
};