"use strict";

const app = require("../../app/index");
const mongoPool = app.locals.mongoPool;

let insertUser = (body, next) => mongoPool.collection("User").insertOne(body,(err, user) =>{
    next(err, user)
});

let modifyUser = (userToFind,body, next) => mongoPool.collection("User").update({_id : mongoPool.ObjectID(userIdToFind)}, body,(err, user) =>{
    next(err, user)
});

let getUser = (userToFind, next) =>{
    mongoPool.collection("User").findOne({_id : mongoPool.ObjectID(userIdToFind)},{$where: "this.active == true"}, (err, user) =>{
        next(err, user)
    });
};

let getUsers = (next) => mongoPool.collection("User").find({ $where: "this.active == true" }).toArray((err, users) =>{
    next(err, users)
});

module.exports = {
    getUser: getUser,
    getUsers: getUsers,
    modifyUser:modifyUser,
    insertUser:insertUser
};
