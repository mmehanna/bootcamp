"use strict";
const UserMdl = require("./UserModel");

let getUser = (req, res) => {
    let userIdToFind = req.params.id;

    UserMdl.getUser(userIdToFind, (err, user) => {
        if (err) return res.status(500).json({});
        return res.status(200).json(user);
    });
};

let insertUser = (req, res) => {
    let body = req.body;

    //json schema validate body

    UserMdl.modifyUser(body, (err, user) => {
        if (err) return res.status(500).json({});
        return res.status(201).json(user);
    });
};
let modifyUser = (req, res) => {
    let userIdToFind = req.params.id;

    //validate userid with regex
    //json schema validate body


    UserMdl.modifyUser(userIdToFind, (err, user) => {
        if (err) return res.status(500).json({});
        return res.status(204).json({});
    });
};

let getUsers = (req, res) => {
    UserMdl.getUsers((err, users) => {
        if (err) return res.status(500).json({});
        return res.status(200).json(users);
    });
};

module.exports = {
    getUser: getUser,
    getUsers: getUsers,
    modifyUser:modifyUser,
    insertUser:insertUser
};
