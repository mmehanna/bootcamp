"use strict";

const app = require("./index");
const UserCtrl = require("../Api/User/UserController");

app.get('/api/user/:id', [UserCtrl.getUsers]);
app.put('/api/user/:id', [UserCtrl.modifyUser]);
app.post('/api/user', [UserCtrl.insertUser]);
app.get('/api/users', [UserCtrl.getUsers]);

app.get('/api/health', (req, res) => res.status(200).json({"ok": "Server is running properly"}));

