"use strict";
const app = module.exports = require('express')();
const bodyParser = require('body-parser');
const config = require("../config/default");

app.use(bodyParser.json());

config.connectMongo((err, mongoPool) => {
    if (err) throw err;

    app.locals.mongoPool = mongoPool;
    require("./routes");
    app.listen(process.env.PORT || 3001);
    console.log("Bootcamp started in ( '" + process.env.NODE_ENV + "' ). Listening on port:" + (process.env.PORT || 3001));
});